﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuManager : MonoBehaviour
{
    [SerializeField]
    private GameObject mMainMenu;

    [SerializeField]
    private GameObject mSpawnMenu;

    private void Start()
    {
        this.mMainMenu.SetActive(true);
        this.mSpawnMenu.SetActive(false);
    }

    public void ToSpawnMenu()
    {
        this.mMainMenu.SetActive(false);
        this.mSpawnMenu.SetActive(true);
    }

    public void ToMainMenu()
    {
        this.mMainMenu.SetActive(true);
        this.mSpawnMenu.SetActive(false);
    }

}
