﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour
{

    [SerializeField]
    private Transform mSpawnContainer;

    [SerializeField]
    private List<GameObject> mSpawnPrefabs;

    [SerializeField]
    private GameObject mPlayer;

    public float mSpawnDistance = 0.45f;


    private List<GameObject> mSpawnedObjects = new List<GameObject>();

    public void Spawn(int index)
    {
        Vector3 spawnPos = this.mPlayer.transform.position + (this.mPlayer.transform.forward * this.mSpawnDistance);
        Quaternion spawnRot = new Quaternion();
        GameObject newObj = GameObject.Instantiate(this.mSpawnPrefabs[index], position: spawnPos, rotation: spawnRot);
        newObj.transform.parent = this.mSpawnContainer;
        this.mSpawnedObjects.Add(newObj);
    }

    public void ClearAll()
    {
        foreach (var obj in this.mSpawnedObjects)
        {
            GameObject.Destroy(obj);
        }
    }

}
