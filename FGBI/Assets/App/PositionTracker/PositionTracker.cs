﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
namespace Assets.App
{
    /// <summary>
    /// A component to easily enable an object to be tracked in the world, for easier positioning in testing.
    /// </summary>
    /// <remarks>Meant to be attached to the gameobject whose position we want to track</remarks>
    public class PositionTracker : MonoBehaviour
    {
        [Tooltip("The prefab to be spawned to maintain data on the object being tracked")]
        [SerializeField]
        [Rename]
        protected GameObject mTrackerPrefab;

        [Tooltip("The thing the tracker data should be facing")]
        [SerializeField]
        [Rename]
        private GameObject mPlayer;


        /// <summary>
        /// Our spawned instance of the tracked prefab
        /// </summary>
        protected GameObject mTrackerInstance;

        /// <summary>
        /// The information manager for the position tracker
        /// </summary>
        protected PositionTrackerManager mPositionTrackerManager;

        public bool isBeingManipulated
        {
            get => mIsBeingManipulated;
        }
        private bool mIsBeingManipulated = false;


        protected virtual void Start()
        {
            this.SpawnPositionTracker();
        }


        public void OnManipulationStart()
        {
            this.mIsBeingManipulated = true;
            if (this.mPositionTrackerManager != null)
            {
                this.mPositionTrackerManager.OnManipulationStart();
            }
        }

        public void OnManipulationEnd()
        {
            this.mIsBeingManipulated = false;
            if (this.mPositionTrackerManager != null)
            {
                this.mPositionTrackerManager.OnManipulationEnd();
            }
        }

        protected void SpawnPositionTracker()
        {
            Vector3 spawnPos = this.transform.position + this.transform.up * 0.05f;
            this.mTrackerInstance = GameObject.Instantiate(this.mTrackerPrefab, spawnPos, new Quaternion());
            this.mTrackerInstance.transform.parent = this.transform;
            this.mPositionTrackerManager = this.mTrackerInstance.GetComponent<PositionTrackerManager>();
            this.mPositionTrackerManager.SetTrackedObject(this.transform);
            this.mPositionTrackerManager.SetPlayerTransform(this.mPlayer.transform);
        }
    }

}