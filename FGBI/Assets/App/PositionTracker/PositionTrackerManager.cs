﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
namespace Assets.App
{
    /// <summary>
    /// A component to handle the position tracker displaying the position information to the user
    /// </summary>
    public class PositionTrackerManager : MonoBehaviour
    {
        /// <summary>
        /// The object we're tracking
        /// </summary>
        private Transform mTrackedObject;

        [Tooltip("The canvas responsible for containing the object's information")]
        [Rename]
        [SerializeField]
        private TextMeshProUGUI mCanvasText;

        /// <summary>
        /// The player the canvas should be facing
        /// </summary>
        private Transform mPlayerTransform;


        /// <summary>
        /// The tracked object's last position
        /// </summary>
        private Vector3 mLastPos;
        /// <summary>
        /// The current position of the tracked object
        /// </summary>
        private Vector3 mCurrentPos
        {
            get
            {
                return this.mTrackedObject.position;
            }
        }

        /// <summary>
        /// Whether or not the object is currently being manipulated
        /// </summary>
        private bool mIsBeingManipulated = false;

        /// <summary>
        /// Whether or not the object is currently being manipulated
        /// </summary>
        public bool isBeingManipulated
        {
            get => this.mIsBeingManipulated;
        }

        /// <summary>
        /// Callback to be called from wherever the manipulation is starting.
        /// </summary>
        /// <remarks>In our case, the event for manipulation beginning is fired from an ObjectManipulator component</remarks>
        public void OnManipulationStart() => this.mIsBeingManipulated = true;

        /// <summary>
        /// Callback to be called from wherever the manipulation is ending.
        /// </summary>
        /// <remarks>In our case, the event for manipulation ending is fired from an ObjectManipulator component</remarks>
        public void OnManipulationEnd() => this.mIsBeingManipulated = false;

        /// <summary>
        /// Update the tracker's tracked object
        /// </summary>
        /// <param name="tracked">The item which we want to be tracking</param>
        public void SetTrackedObject(Transform tracked)
        {
            this.mTrackedObject = tracked;
            this.mLastPos = this.mTrackedObject.position;
            SetCanvasText(this.mLastPos);
        }

        /// <summary>
        /// Update the transform of where we want to face
        /// </summary>
        /// <param name="tracked">The item which we want to be facing</param>
        public void SetPlayerTransform(Transform player)
        {
            this.mPlayerTransform = player;
        }

        void Start()
        {
            if (this.mTrackedObject != null)
            {
                this.mLastPos = this.mTrackedObject.position;
                SetCanvasText(this.mLastPos);
                UpdateCanvasRotation();
            }
        }

        void Update()
        {
            if (mIsBeingManipulated)
            {
                if (TrackedObjectsPositionIsDifferent())
                {
                    UpdateCanvasRotation();
                    UpdateCanvasText();
                    UpdateTrackedObjectLastKnownPosition();
                }
            }
        }

        #region HELPERS
        /// <summary>
        /// Helper to compare whether the tracked object's position
        /// </summary>
        /// <returns></returns>
        private bool TrackedObjectsPositionIsDifferent() => this.mCurrentPos != this.mLastPos;

        /// <summary>
        /// Update the text which is shown on the canvas
        /// </summary>
        private void UpdateCanvasText() => SetCanvasText(this.mCurrentPos);

        /// <summary>
        /// Update the rotation of the canvas to face the player
        /// </summary>
        private void UpdateCanvasRotation()
        {
            this.mCanvasText.transform.parent.rotation = Quaternion.LookRotation(this.mCanvasText.transform.position - this.mPlayerTransform.position);
        }


        /// <summary>
        /// Update our last known position of where the tracked object was
        /// </summary>
        private void UpdateTrackedObjectLastKnownPosition() =>
            this.mLastPos = this.mCurrentPos;

        /// <summary>
        /// A helper to update the canvas text
        /// </summary>
        /// <param name="position">The vector3 from which the canvas text should be updated</param>
        private void SetCanvasText(Vector3 position)
        {
            string x = FormatDecimalPlace(position.x, 3);
            string y = FormatDecimalPlace(position.y, 3);
            string z = FormatDecimalPlace(position.z, 3);
            string text = $"{x}, {y}, {z}";
            Debug.Log($"Set canvas text to {text}");
            this.mCanvasText.SetText(text);
        }

        /// <summary>
        /// A helper to return a formatted string made from a decimal, where the stringified number is made up of the provided number of decimals
        /// </summary>
        /// <param name="num">The number to return a string of</param>
        /// <param name="numDecimals">The number of decimals the string should contain</param>
        /// <returns></returns>
        private string FormatDecimalPlace(float num, int numDecimals)
        {
            string formatString = "0.";
            for (int i = 0; i < numDecimals; i++)
            {
                formatString += "0";
            }
            return num.ToString(formatString);
        }
        #endregion
    }

}