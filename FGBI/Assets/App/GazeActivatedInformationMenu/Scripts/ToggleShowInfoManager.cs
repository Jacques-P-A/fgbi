﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Assets.App
{

    public class ToggleShowInfoManager : MonoBehaviour
    {
        [SerializeField]
        [Rename]
        private GameObject mThingToTrack;

        public bool positionTrackerIsActive
        {
            get
            {
                return this.mPositionTracker.enabled;
            }
        }

        private HidablePositionTracker mPositionTracker;

        private void Start()
        {
            this.mPositionTracker = this.mThingToTrack.GetComponent<HidablePositionTracker>();
        }

        public void TogglePositionTracker()
        {
            if (this.mPositionTracker.isHidden)
            {
                this.mPositionTracker.Reveal();
            }
            else
            {
                this.mPositionTracker.Hide();
            }
        }

    }

}