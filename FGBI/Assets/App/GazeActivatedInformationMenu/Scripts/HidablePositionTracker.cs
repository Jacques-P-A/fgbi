﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Assets.App
{

    public class HidablePositionTracker : PositionTracker, IHidable
    {
        public bool isHidden
        {
            get => this.mTrackerInstance == null;
        }

        public void Hide()
        {
            GameObject.Destroy(this.mTrackerInstance);
        }

        public void Reveal()
        {
            SpawnPositionTracker();
        }


        protected override void Start()
        {

        }

    }

}