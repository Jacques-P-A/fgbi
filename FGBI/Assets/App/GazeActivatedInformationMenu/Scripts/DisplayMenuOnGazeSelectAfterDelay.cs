﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Assets.App
{
    /// <summary>
    /// Display a menu on gaze select after a delay
    /// </summary>
    public class DisplayMenuOnGazeSelectAfterDelay : AnimateOnGazeSelection
    {
        [Tooltip("The menu to display")]
        [SerializeField]
        [Rename]
        private GameObject mMenu;

        [Tooltip("The player to face")]
        [SerializeField]
        [Rename]
        private GameObject mPlayer;

        [Tooltip("The progress bar to give visual feedback of gaze selection progress")]
        [SerializeField]
        [Rename]
        private ProgressBar mProgressBar;


        public float delayUntilMenuDisplay;

        private float mTimer = 0.0f;

        /// <summary>
        /// Keep track of the point at which the menu is fully displayed
        /// </summary>
        private bool mIsMenuFullyDisplayed = false;
        /// <summary>
        /// Keep track of the point at which the menu is fully hidden
        /// </summary>
        private bool mIsMenuFullyHidden = false;

        void Start()
        {
            this.HideMenu();
        }

        #region INHERITED ANIMATION FUNCTIONALITIES
        protected override bool IsAnimationComplete()
        {
            return mIsMenuFullyDisplayed;
        }
        protected override void Animate()
        {
            //break if the cube is being manipulated
            if (this.GetComponent<HidablePositionTracker>().isBeingManipulated)
            {
                return;
            }
            this.mProgressBar.UpdateProgress(this.mTimer / this.delayUntilMenuDisplay);
            if (this.mTimer < this.delayUntilMenuDisplay)
            {
                UpdateTimer();

                return;
            }
            //TODO disable menu display on interaction with cube
            DisplayMenu();
        }

        protected override void ResetAnimation()
        {
            bool menuIsLookedAt = this.mMenu.GetComponent<ImLookedAt>().IsLookedAt;
            //if neither the menu or the current gameobject is looked at
            if (!this.IsLookedAt && !menuIsLookedAt)
            {
                //...then hide the menu
                this.HideMenu();
            }
        }

        protected override void OnAnimationComplete()
        {
        }
        #endregion
        #region HELPERS


        private IEnumerator FadeOutMenuSlowlyAndResetBaseParameters() => new WaitUntil(delegate () {
            this.HideMenuABit();
            if (this.mIsMenuFullyHidden) ResetBaseParameters();
            return this.mIsMenuFullyHidden;
        });

        private void HideMenuABit()
        {
            //TODO
            this.mMenu.SetActive(false);
            this.mIsMenuFullyHidden = true;
        }

        private void ResetBaseParameters()
        {
            this.mTimer = 0.0f;
            this.mMenu.SetActive(false);
            this.mIsMenuFullyDisplayed = false;
        }

        private void DisplayMenu()
        {
            Debug.Log("The menu is displayed");

            Vector3 forward = Vector3.Normalize(this.transform.position - this.mPlayer.transform.position);
            Vector3 right = Vector3.Normalize(Vector3.Cross(Vector3.up, forward));
            Vector3 down = -Vector3.Normalize(Vector3.Cross(forward, right));
            Vector3 spawnPos = this.mPlayer.transform.position + (forward * 0.4f) + (right * 0.05f) + (down * 0.05f);
            this.mMenu.gameObject.transform.position = spawnPos;

            this.mMenu.transform.rotation = Quaternion.LookRotation(this.mMenu.transform.position - this.mPlayer.transform.position);

            this.mMenu.SetActive(true);
            this.mIsMenuFullyDisplayed = true;
        }

        private void HideMenu()
        {
            Debug.Log("The menu is hidden");
            //fade out slowly and then reset base parameters
            StartCoroutine(FadeOutMenuSlowlyAndResetBaseParameters());
        }

        private void UpdateTimer() => this.mTimer += Time.fixedDeltaTime;
        #endregion
    }

}