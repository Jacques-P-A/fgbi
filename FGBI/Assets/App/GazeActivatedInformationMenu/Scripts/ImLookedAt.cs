﻿using Microsoft.MixedReality.Toolkit.Input;

using UnityEngine;

namespace Assets.App
{

    [RequireComponent(typeof(EyeTrackingTarget))]
    public class ImLookedAt : MonoBehaviour
    {
        private bool mIsLookedAt = false;
        public bool IsLookedAt
        {
            get
            {
                return this.mIsLookedAt;
            }
        }

        public void OnLookAtStart()
        {
            this.mIsLookedAt = true;
        }

        public void OnLookAway()
        {
            this.mIsLookedAt = false;
        }

    }
}
