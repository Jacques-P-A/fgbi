﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class ProgressBar : MonoBehaviour
{
    [SerializeField]
    [Rename]
    private Slider mSlider;

    private CanvasGroup mCanvasGroup;


    private float mTimeOfLastInput = 0.0f;
    public float delayUntilResetWithoutInput;

    private bool mIsResettingSlider = false;

    private float mProgress
    {
        get => mSlider.value;
    }

    private void Start()
    {
        this.mCanvasGroup = this.GetComponent<CanvasGroup>();
        this.mCanvasGroup.alpha = 0.0f;
    }

    private void Update()
    {
        FadeOutSliderIfInputTakesTooLong();
    }

    private void FadeOutSliderIfInputTakesTooLong()
    {
        if (this.mProgress > 0.0f && mProgress < 1.0)
        {
            float timeSinceLastInput = Time.time - mTimeOfLastInput;
            if (timeSinceLastInput >= delayUntilResetWithoutInput)
            {
                if (!mIsResettingSlider)
                {
                    Debug.Log("Resetting slider");
                    StartCoroutine(FadeOut());
                }
            }
        }
    }

    public void UpdateProgress(float progress)
    {
        //if the slider's value is 0, reveal the slider
        if (this.mSlider.value == 0)
        {
            //reveal the slider
            StartCoroutine(FadeIn());
        }

        mSlider.value = progress;
        this.mTimeOfLastInput = Time.time;
        if (mSlider.value >= 1)
        {
            StartCoroutine(FadeOut());
        }
    }

    private IEnumerator FadeOut()
    {
        this.mIsResettingSlider = true;
        //Debug.Log("Fading out");
        //hide the slider
        while (this.mCanvasGroup.alpha > 0)
        {
            this.mCanvasGroup.alpha -= Time.deltaTime * 2.0f;
            yield return null;
        }
        //reset the mSlider value
        this.mSlider.value = 0.0f;
        this.mIsResettingSlider = false;
        yield return null;
    }


    private IEnumerator FadeIn()
    {
        //Debug.Log("Fading out");
        //hide the slider
        while (this.mCanvasGroup.alpha < 1.0f)
        {
            this.mCanvasGroup.alpha += Time.deltaTime * 2.0f;
            yield return null;
        }
        yield return null;
    }
}


//public class ProgressBar : MonoBehaviour
//{
//    [SerializeField]
//    [Rename]
//    private Slider mSlider;

//    private CanvasGroup mCanvasGroup;

//    private void Start()
//    {
//        this.mCanvasGroup = this.GetComponent<CanvasGroup>();
//    }

//    private float mProgress = 0.0f;

//    private void Update()
//    {
//        float rate = Time.deltaTime;
//        if (mProgress < 1.0f)
//        {
//            Debug.Log($"Progress {mProgress * 100.0f}%");
//            mProgress += rate;
//            this.UpdateProgress(mProgress);
//        }
//    }

//    public void UpdateProgress(float progress)
//    {
//        //if the slider's value is 0, reveal the slider
//        if (this.mSlider.value == 0)
//        {
//            //reveal the slider
//            this.mCanvasGroup.alpha = 1.0f;
//        }

//        mSlider.value = progress;
//        if (mSlider.value >= 1)
//        {
//            StartCoroutine(FadeOut());
//        }
//    }

//    private IEnumerator FadeOut()
//    {
//        //Debug.Log("Fading out");
//        //hide the slider
//        while (this.mCanvasGroup.alpha > 0)
//        {
//            this.mCanvasGroup.alpha -= Time.deltaTime * 2.0f;
//            yield return null;
//        }
//        //reset the mSlider value
//        this.mSlider.value = 0.0f;
//        this.mProgress = 0.0f;
//        yield return null;
//    }
//}
