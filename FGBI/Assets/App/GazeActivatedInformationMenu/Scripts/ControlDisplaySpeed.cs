﻿using Microsoft.MixedReality.Toolkit.UI;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
namespace Assets.App
{

    public class ControlDisplaySpeed : MonoBehaviour
    {
        [SerializeField]
        DisplayMenuOnGazeSelectAfterDelay mMenuDisplay;

        [SerializeField]
        TextMeshProUGUI mText;

        private void Start()
        {
            PinchSlider ps = this.GetComponent<PinchSlider>();
            float newValue = ps.SliderValue * 10.0f;
            mMenuDisplay.delayUntilMenuDisplay = newValue;
            mText.text = $"{newValue}";
        }

        public void OnValueUpdated()
        {
            PinchSlider ps = this.GetComponent<PinchSlider>();
            float newValue = ps.SliderValue * 10.0f;
            mMenuDisplay.delayUntilMenuDisplay = newValue;
            mText.text = $"{newValue}";
        }

    }
}
