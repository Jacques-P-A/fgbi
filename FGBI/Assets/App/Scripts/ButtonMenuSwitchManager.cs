﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Assets.App
{

    public class ButtonMenuSwitchManager : MonoBehaviour
    {
        [Tooltip("The gameobject to use as menu")]
        [SerializeField]
        [Rename]
        private GameObject mMenu;

        [Tooltip("The gameobject to use as gaze-activated button")]
        [SerializeField]
        [Rename]
        private GameObject mButton;

        /// <summary>
        /// The IHidable instance associated to the menu gameobject
        /// </summary>
        private IHidable mMenuHidable;

        /// <summary>
        /// The IHidable instance associated to the button gameobject
        /// </summary>
        private IHidable mButtonHidable;

        /// <summary>
        /// The currently active display (either button or menu)
        /// </summary>
        public GameObject activeDisplay
        {
            get
            {
                return this.mActiveDisplay;
            }
        }

        /// <summary>
        /// The currently active display (either button or menu)
        /// </summary>
        private GameObject mActiveDisplay;

        void Start()
        {
            this.mMenuHidable = this.mMenu.GetComponent<IHidable>();
            this.mButtonHidable = this.mButton.GetComponent<IHidable>();

            this.mActiveDisplay = this.mButton;
            this.mMenuHidable.Hide();
        }

        /// <summary>
        /// Reveal the button and hide the menu
        /// </summary>
        public void ToggleButton()
        {
            this.Toggle(fromHidable: this.mMenuHidable, toHidable: this.mButtonHidable, toObj: this.mButton);
        }

        /// <summary>
        /// Reveal the menu and hide the button
        /// </summary>
        public void ToggleMenu()
        {
            this.Toggle(fromHidable: this.mButtonHidable, toHidable: this.mMenuHidable, toObj: this.mMenu);
        }

        #region HELPERS
        private void Toggle(IHidable fromHidable, IHidable toHidable, GameObject toObj)
        {
            toObj.SetActive(true);

            toHidable.Reveal();
            fromHidable.Hide();
            this.mActiveDisplay = toObj;
        }
        #endregion
    }

}