﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.App
{
    /// <summary>
    /// Class to handle material swapping from active to inactive, and enforce the use of meshrenderer for the gameobject
    /// </summary>
    [RequireComponent(typeof(MeshRenderer))]
    public class ActiveInactiveMaterialSwap : MonoBehaviour
    {
        [Tooltip("The material to be shown when the component is toggled")]
        [SerializeField]
        [Rename]
        private Material mActiveMaterial;

        [Tooltip("The material to be shown when the component isn't toggled")]
        [SerializeField]
        [Rename]
        private Material mInactiveMaterial;

        /// <summary>
        /// The currently enabled material
        /// </summary>
        public Material CurrentMaterial
        {
            get
            {
                return this.mMeshRenderer.material;
            }
        }
        /// <summary>
        /// A reference to the gameobject's meshrenderer component
        /// </summary>
        private MeshRenderer mMeshRenderer;

        private void Start()
        {
            this.mMeshRenderer = this.transform.GetComponent<MeshRenderer>();
        }

        /// <summary>
        /// Set as current material active material
        /// </summary>
        public void ToggleActiveMaterial()
        {
            this.mMeshRenderer.material = this.mActiveMaterial;
        }

        /// <summary>
        /// Set as current material inactive material
        /// </summary>
        public void ToggleInactiveMaterial()
        {
            this.mMeshRenderer.material = this.mInactiveMaterial;
        }

        /// <summary>
        /// Whether or not the current material is the active material
        /// </summary>
        public bool CurrentMaterialIsActiveMaterial()
        {
            return this.mMeshRenderer.sharedMaterial == this.mActiveMaterial;
        }

        /// <summary>
        /// Whether or not the current material is the inactive material
        /// </summary>
        public bool CurrentMaterialIsInactiveMaterial()
        {
            return this.mMeshRenderer.sharedMaterial == this.mInactiveMaterial;
        }
    }
}
