﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.App
{

    public class HideMenu : MonoBehaviour, IHidable
    {
        [Tooltip("The menu to expand to desired dimensions on button gaze.")]
        [SerializeField]
        [Rename]
        private GameObject mMenuQuad;

        [Tooltip("The menu's button collection to activate on menu expansion.")]
        [SerializeField]
        [Rename]
        private GameObject mButtonCollection;

        [Tooltip("The rate at which the menu's scale should change.")]
        [SerializeField]
        [Rename]
        private float mGrowthRate;

        [Tooltip("The end dimensions the menu should have at the end of its expansion")]
        [SerializeField]
        [Rename]
        private Vector3 mDesiredDimensions;


        /// <summary>
        /// What to come within such that the menu's dimensions are more or less what we want
        /// </summary>
        private float mMarginOfError = 1e-4f;


        public void Reveal()
        {
            StartCoroutine(OpenMenu());
        }

        public void Hide()
        {
            this.gameObject.SetActive(false);
        }

        /// <summary>
        /// Menu expansion coroutine; executes until menu is of the desired dimensions
        /// </summary>
        private IEnumerator OpenMenu()
        {
            //Scale up the menu
            yield return new WaitUntil(MenuIsOfRightDimensions);
            //Activate its buttons
            yield return new WaitUntil(ButtonsAreVisible);
        }

        /// <summary>
        /// Whether or not
        /// </summary>
        /// <remarks></remarks>
        private bool ButtonsAreVisible()
        {
            this.mButtonCollection.SetActive(true);
            return this.mButtonCollection.activeSelf;
        }


        /// <summary>
        /// Whether or not the menu's current dimensions are what we want
        /// </summary>
        /// <remarks>Also handles the scaling of the menu until it is of the right dimensions</remarks>
        private bool MenuIsOfRightDimensions()
        {
            DIMENSIONS[] allDimensions = (DIMENSIONS[])Enum.GetValues(typeof(DIMENSIONS));

            foreach (var dimension in allDimensions)
            {
                Scale(dimension);
            }

            return this.mMenuQuad.transform.localScale == this.mDesiredDimensions;
        }

        /// <summary>
        /// Scale a given dimension of our menu with respect to our desired dimensions and our current dimensions
        /// </summary>
        /// <param name="dimension">The dimension to scale, if necessary</param>
        private void Scale(DIMENSIONS dimension)
        {
            float current = GetMenuDimensionLength(dimension);
            float desired = GetDesiredDimensionLength(dimension);
            float differenceBetweenCurrentAndDesired = Mathf.Abs(current - desired);
            //if the current and desired lengths are different
            if (differenceBetweenCurrentAndDesired != 0.0f)
            {
                //if the difference between current and desired is greater than margin of error...
                if (differenceBetweenCurrentAndDesired > this.mMarginOfError)
                {
                    float change = mGrowthRate * Mathf.Pow(Time.fixedDeltaTime, 2);
                    //then either add or subtract from value
                    if (current < desired)
                    {
                        SetMenuLocalScale(dimension, current + change);
                    }
                    else if (current > desired)
                    {
                        SetMenuLocalScale(dimension, current - change);
                    }
                }
                //else if the difference between current and desired is less than margin of error...
                else if (differenceBetweenCurrentAndDesired < this.mMarginOfError)
                {
                    //...then clamp the value to the desired value
                    SetMenuLocalScale(dimension, desired);
                }
            }
            //else if the difference between current and desired lengths is 0 do nothing 
        }

        #region HELPERS
        private enum DIMENSIONS
        {
            X = 0, Y, Z
        };

        /// <summary>
        /// Helper to set the menu transform's local scale for a given dimension
        /// </summary>
        /// <param name="dimension">the concerned dimension (X, Y, Z)</param>
        /// <param name="value">the new value to assign to the given dimension</param>
        private void SetMenuLocalScale(DIMENSIONS dimension, float value)
        {
            Vector3 newValue = this.mMenuQuad.transform.localScale;

            switch (dimension)
            {
                case DIMENSIONS.X:
                    {
                        newValue.x = value;
                        break;
                    }
                case DIMENSIONS.Y:
                    {
                        newValue.y = value;
                        break;
                    }
                default:
                    {
                        newValue.z = value;
                        break;
                    }
            }
            this.mMenuQuad.transform.localScale = newValue;
        }

        /// <summary>
        /// Get the menu's current length for a given dimension
        /// </summary>
        /// <param name="dimension">the concerned dimension (X, Y, Z)</param>
        private float GetMenuDimensionLength(DIMENSIONS dimension)
        {
            return ValueFromVectorDimension(this.mMenuQuad.transform.localScale, dimension);
        }

        /// <summary>
        /// Get the desired length for a given dimension
        /// </summary>
        /// <param name="dimension">the concerned dimension (X, Y, Z)</param>
        private float GetDesiredDimensionLength(DIMENSIONS dimension)
        {
            return ValueFromVectorDimension(this.mDesiredDimensions, dimension);
        }

        /// <summary>
        /// Return the corresponding vector's value for the given dimension
        /// </summary>
        private float ValueFromVectorDimension(Vector3 vector, DIMENSIONS dimension)
        {
            switch (dimension)
            {
                case DIMENSIONS.X:
                    {
                        return vector.x;
                    }
                case DIMENSIONS.Y:
                    {
                        return vector.y;
                    }
                default:
                    {
                        return vector.z;
                    }
            }
        }

        #endregion
    }
}
