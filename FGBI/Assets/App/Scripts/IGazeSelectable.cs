﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Assets.App
{
    public interface IGazeSelectable
    {
        void OnLookAtStart();

        void OnLookAway();
    }

}