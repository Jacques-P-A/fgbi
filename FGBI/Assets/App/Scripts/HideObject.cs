﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.App
{
    public class HideObject : IHidable
    {

        [SerializeField]
        private List<MeshRenderer> mThingsToHide;

        public virtual void Hide()
        {
            throw new System.NotImplementedException();
        }

        protected void HideItem()
        {

        }

        public virtual void Reveal()
        {
            throw new System.NotImplementedException();
        }

    }
}

