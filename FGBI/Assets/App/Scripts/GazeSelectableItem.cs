﻿using Microsoft.MixedReality.Toolkit.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.App
{
    /// <summary>
    /// Ensure the object knows it's being looked at.
    /// </summary>
    /// <remarks>Any gameobject which is using this is expected to set the EyeTrackingTarget OnLookAtStart and OnLookAway events to the corresponding functions of this class with the same name.</remarks>
    [RequireComponent(typeof(EyeTrackingTarget))]
    public abstract class GazeSelectableItem : MonoBehaviour, IGazeSelectable
    {

        /// <summary>
        /// Whether or not the item being animated is currently looked at
        /// </summary>
        private bool mIsLookedAt = false;

        /// <summary>
        /// Whether or not the item being animated is currently looked at
        /// </summary>
        protected bool IsLookedAt
        {
            get
            {
                return mIsLookedAt;
            }
        }

        /// <summary>
        /// What to do when the item is looked at.
        /// </summary>
        public virtual void OnLookAtStart()
        {
            this.mIsLookedAt = true;
        }

        /// <summary>
        /// What to do when the item is no longer looked at
        /// </summary>
        public virtual void OnLookAway()
        {
            this.mIsLookedAt = false;
        }

    }
}
