﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Assets.App
{
    public sealed class HideButton : MonoBehaviour, IHidable
    {
        [Tooltip("Everything we want to fade out during a transition")]
        [SerializeField]
        [Rename]
        private List<MeshRenderer> mThingsToHide;

        /// <summary>
        /// The speed at which the fade out should occur
        /// </summary>
        private float mFadeSpeed = 1.0f;


        /// <summary>
        /// Hide everything we want to hide about the button
        /// </summary>
        public void Hide()
        {
            StartCoroutine(HideEachItemAndDisableComponent());
        }

        private IEnumerator HideEachItemAndDisableComponent()
        {
            //foreach (var item in this.mThingsToHide)
            //{
            //    yield return new WaitUntil(delegate () {
            //        HideItem(item);
            //        return IsFullyHidden(item);
            //    });
            //}
            this.gameObject.SetActive(false);
            yield return null;

        }


        /// <summary>
        /// Reveal everything we want to reveal about the button
        /// </summary>
        public void Reveal()
        {
            //for every thing we have to reveal...
            foreach (var item in this.mThingsToHide)
            {
                //... reveal it progressively until it's fully visible
                StartCoroutine(new WaitUntil(delegate () {
                    RevealItem(item);
                    return IsFullyVisible(item);
                }));
            }
        }

        #region HELPERS

        private void HideItem(MeshRenderer item)
        {
            Color c = item.material.color;
            if (c.a > 0)
            {
                float newAlpha = c.a - (this.mFadeSpeed * Time.fixedDeltaTime);
                Color newColor = new Color(c.r, c.g, c.b, newAlpha);
                item.material.color = newColor;
            }
            else if (c.a < 0)
            {
                float newAlpha = 0.0f;
                Color newColor = new Color(c.r, c.g, c.b, newAlpha);
                item.material.color = newColor;
            }
        }

        private bool IsFullyHidden(MeshRenderer item) => item.material.color.a == 0.0f;

        private bool IsFullyVisible(MeshRenderer item) => item.material.color.a == 1.0f;

        public void RevealItem(MeshRenderer item)
        {
            Color c = item.material.color;
            if (c.a < 1)
            {
                float newAlpha = c.a + (this.mFadeSpeed * Time.fixedDeltaTime);
                Color newColor = new Color(c.r, c.g, c.b, newAlpha);
                item.material.color = newColor;
            }
            else if (c.a > 1)
            {
                float newAlpha = 1.0f;
                Color newColor = new Color(c.r, c.g, c.b, newAlpha);
                item.material.color = newColor;
            }
        }
        #endregion
    }
}
