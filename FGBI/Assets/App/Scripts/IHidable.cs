﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Assets.App
{
    public interface IHidable
    {
        /// <summary>
        /// The behaviour the item should have when we reveal it
        /// </summary>
        void Reveal();
        /// <summary>
        /// The behaviour the item should have when we hide it
        /// </summary>
        void Hide();
    }
}
