﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.App
{
    /// <summary>
    /// On gaze selection perform following animation, in order: <list type="number">
    /// <item>Spin the spinner</item>
    /// <item>Highlight background</item>
    /// </list>
    /// </summary>
    /// <remarks>if the user looks away, the animation is reset</remarks>

    public class AnimateButtonSpinnerOnGazeSelect : AnimateOnGazeSelection
    {
        #region ANIMATION PARAMETERS
        [SerializeField]
        [Tooltip("The button's background quad which flashes on spinner completion")]
        [Rename]
        private GameObject mSpinnerBackground;

        /// <summary>
        /// the spinner
        /// </summary>
        [SerializeField]
        [Tooltip("The spinner which turns about the button on gaze")]
        [Rename]
        private GameObject mSpinner;


        /// <summary>
        /// the background's material swapper
        /// </summary>
        private ActiveInactiveMaterialSwap mBackgroundMaterialSwap;


        /// <summary>
        /// the spinner's material swapper
        /// </summary>
        private ActiveInactiveMaterialSwap mSpinnerMaterialSwap;

        /// <summary>
        /// Trail renderer used to give the "tail" impression to the spinner
        /// </summary>
        private TrailRenderer mSpinnerTrailRenderer;

        /// <summary>
        /// The cursor's position before animation start
        /// </summary>
        private Vector3 mInitialCursorPos = new Vector3(0.0f, 0.5f, -0.5f);

        /// <summary>
        /// The time it takes for one cycle of the animation to complete
        /// </summary>
        /// <remarks>TODO: make consistent with seconds</remarks>
        [Tooltip("The time it should take for the animation to complete")]
        [Rename]
        public float mAnimationDuration;

        /// <summary>
        /// The proportion of the animation duration which should be attributed to the spinner. In other words, the percentage of time which should be spent making the spinner spin.
        /// </summary>
        private readonly float mSpinnerAnimationProportion = 0.7f;
        /// <summary>
        /// The duration of the spinner animation
        /// </summary>
        private float mSpinnerAnimationDuration;
        /// <summary>
        /// A timer to manage the time spent on each part of the animation
        /// </summary>
        private float mTimer = 0.0f;

        /// <summary>
        /// the speed at which the rotation is taking place
        /// </summary>
        /// <remarks>Determined by animation duration</remarks>
        private float mRotationSpeed;

        /// <summary>
        /// The number of times the item should flash for the second part of the animation
        /// </summary>
        /// <remarks>Changing this value may break the flash animation</remarks>
        private int mFlashRepetitions = 2;
        #endregion

        protected virtual void Start()
        {
            this.mSpinner.transform.localPosition = mInitialCursorPos;
            mSpinnerTrailRenderer = mSpinner.GetComponent<TrailRenderer>();

            this.mBackgroundMaterialSwap = this.mSpinnerBackground.GetComponent<ActiveInactiveMaterialSwap>();
            this.mSpinnerMaterialSwap = this.mSpinner.GetComponent<ActiveInactiveMaterialSwap>();

            mSpinnerTrailRenderer.enabled = false;

            this.mSpinnerAnimationDuration = this.mAnimationDuration * this.mSpinnerAnimationProportion;

            this.mRotationSpeed = (360.0f / this.mSpinnerAnimationDuration) * Time.fixedDeltaTime;

        }

        #region OVERRIDDEN ANIMATION FUNCTIONS
        /// <summary>
        /// Animate the button; first make spinner spin, and then flash the background
        /// </summary>
        protected override void Animate()
        {
            if (this.mTimer < this.mSpinnerAnimationDuration)
            {
                SpinSpinner();
            }
            else //timer is >= spinner animation duration
            {
                FlashBackground();
            }
            this.mTimer += Time.fixedDeltaTime;
        }

        protected override void ResetAnimation()
        {
            //reset spinner params
            this.mSpinnerTrailRenderer.enabled = false;

            this.mSpinnerMaterialSwap.ToggleInactiveMaterial();

            //reset background params
            this.mBackgroundMaterialSwap.ToggleInactiveMaterial();

            //reset the item
            this.mSpinner.transform.localPosition = mInitialCursorPos;

            //reset animation params
            this.mTimer = 0.0f;
        }

        protected override bool IsAnimationComplete()
        {
            return this.mTimer >= this.mAnimationDuration;
        }

        protected override void OnAnimationComplete()
        {
            Debug.Log("The animation is complete for the button spinner");
        }

        #endregion

        #region HELPERS
        /// <summary>
        /// A helper to handle the spinning of the spinner, for the first part of the animation
        /// </summary>
        private void SpinSpinner()
        {
            //animation start setup
            if (!this.mSpinnerTrailRenderer.enabled) this.mSpinnerTrailRenderer.enabled = true;
            if (!this.mSpinnerMaterialSwap.CurrentMaterialIsActiveMaterial()) this.mSpinnerMaterialSwap.ToggleActiveMaterial();

            //animate the item
            mSpinner.transform.RotateAround(this.transform.localPosition, -Vector3.forward, this.mRotationSpeed);
        }

        /// <summary>
        /// A helper to handle the flashing of the background, for the second part of the animation
        /// </summary>
        private void FlashBackground()
        {
            float flashTimer = ((this.mTimer - this.mSpinnerAnimationDuration) / this.mFlashRepetitions / 2.0f) * 16.0f / (this.mAnimationDuration - this.mSpinnerAnimationDuration);
            int increment = (int)flashTimer % 2;
            if (increment == 0)
            {
                //set background material to active material
                if (!this.mBackgroundMaterialSwap.CurrentMaterialIsActiveMaterial())
                {
                    this.mBackgroundMaterialSwap.ToggleActiveMaterial();
                }

            }
            else //increment % 2 == 1
            {
                //set background material to inactive material
                if (!this.mBackgroundMaterialSwap.CurrentMaterialIsInactiveMaterial())
                {
                    this.mBackgroundMaterialSwap.ToggleInactiveMaterial();
                }
            }
        }
        #endregion
    }
}
