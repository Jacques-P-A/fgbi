﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.App
{
    /// <summary>
    /// Make the item spin once about the Y axis when gaze selected.
    /// </summary>
    /// <remarks>if the user looks away, the animation is reset</remarks>
    sealed class SpinOnGazeSelect : AnimateOnGazeSelection
    {
        #region INITIAL TRANSFORM PARAMETERS
        /// <summary>
        /// The object's position before animation start
        /// </summary>
        private Vector3 mInitialPos;
        /// <summary>
        /// the object's rotation before animation start
        /// </summary>
        private Quaternion mInitialRotation;
        /// <summary>
        /// the object's scale before animation start
        /// </summary>
        private Vector3 mInitialScale;
        #endregion

        #region SPIN ANIMATION PARAMETERS
        /// <summary>
        /// The speed at which to rotate in the animation
        /// </summary>
        public float rotationSpeed;

        /// <summary>
        /// The amount we've rotated the object by
        /// </summary>
        private float mRotatedBy = 0;

        /// <summary>
        /// The rotation at which to stop turning
        /// </summary>
        private float mGoalRotation = 360.0f;
        #endregion

        private void Start()
        {
            this.mInitialPos = this.transform.position;
            this.mInitialRotation = this.transform.rotation;
            this.mInitialScale = this.transform.localScale;
        }

        protected override void Animate()
        {
            //animate the item
            float rotateBy = this.rotationSpeed * Time.deltaTime;
            this.transform.Rotate(this.transform.up, rotateBy);

            //update animation params
            this.mRotatedBy += rotateBy;
        }

        protected override void ResetAnimation()
        {
            //reset the item
            this.transform.position = this.mInitialPos;
            this.transform.rotation = this.mInitialRotation;
            this.transform.localScale = this.mInitialScale;

            //reset animation params
            this.mRotatedBy = 0;
        }

        protected override bool IsAnimationComplete()
        {
            //Let the animation be complete for amount rotated by is greater than or equal to our goal rotation
            return this.mRotatedBy >= mGoalRotation;
        }

        protected override void OnAnimationComplete()
        {
            Debug.Log("Animation complete! - doing very important things");
        }
    }
}
