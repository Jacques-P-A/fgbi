﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.App
{
    /// <summary>
    /// A framework for easy animation on gaze selection of an item
    /// </summary>
    public abstract class AnimateOnGazeSelection : GazeSelectableItem
    {
        public override void OnLookAtStart()
        {
            base.OnLookAtStart();
            this.StartCoroutine(StartAnimation());
        }

        private void OnDisable()
        {
            StopAllCoroutines();
        }

        private void OnDestroy()
        {
            StopAllCoroutines();
        }

        public override void OnLookAway()
        {
            base.OnLookAway();
            this.StopAnimation();
        }

        /// <summary>
        /// Whether or not the animation is complete
        /// </summary>
        protected virtual bool IsAnimationComplete()
        {
            return true;
        }

        /// <summary>
        /// What to do to animate the selectable
        /// </summary>
        protected virtual void Animate()
        {
        }

        /// <summary>
        /// Reset the animated item to initial settings
        /// </summary>
        /// <remarks>This function is called both on animation completion, and on look away, even if the animation is not complete</remarks>
        protected virtual void ResetAnimation()
        {
        }

        /// <summary>
        /// What to do when the animation completes
        /// </summary>
        protected virtual void OnAnimationComplete()
        {
        }

        /// <summary>
        /// Start animation
        /// </summary>
        /// <remarks>Triggered on look at</remarks>
        private IEnumerator StartAnimation()
        {
            bool animationIsComplete = false;
            //Wait until either the animation completes or the user looks away
            yield return new WaitUntil(delegate () {
                animationIsComplete = IsAnimationComplete();
                bool shouldAnimate = !animationIsComplete && this.IsLookedAt;
                if (shouldAnimate) Animate();
                return !shouldAnimate;
            });
            //if the animation completed, perform the OnAnimationComplete
            if (animationIsComplete)
            {
                OnAnimationComplete();
            }

            //then reset it to the initial settings
            this.ResetAnimation();
        }

        /// <summary>
        /// Stop animation
        /// </summary>
        /// <remarks>Triggered on look away</remarks>
        private void StopAnimation()
        {
            //reset
            this.ResetAnimation();
        }
    }
}
