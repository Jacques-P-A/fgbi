﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.App
{

    /// <summary>
    /// A class to expand an MRTK menu on button gaze
    /// </summary>
    public class OpenMenuOnGazeSelect : AnimateButtonSpinnerOnGazeSelect
    {
        [Tooltip("The component managing the state change from menu to button")]
        [Rename]
        [SerializeField]
        private ButtonMenuSwitchManager mManager;


        protected override void OnAnimationComplete()
        {
            mManager.ToggleMenu();
        }

    }
}
