﻿using Microsoft.MixedReality.Toolkit.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GazeSelectionAnimation : MonoBehaviour
{
    // object acting like a cursor
    [SerializeField]
    [Rename()]
    private Transform mFillingCursor;

    // timing of the button activation
    [SerializeField]
    [Rename()]
    private float mTiming;

    // Get the interactable component of the MRTK package
    [SerializeField]
    [Rename()]
    private Interactable mInteractable;

    // Material for enabled cursor
    [SerializeField]
    [Rename()]
    private Material mEnabledMaterial;

    // Material for disabled cursor
    [SerializeField]
    [Rename()]
    private Material mDisabledMaterial;

    // Material for button activation
    [SerializeField]
    [Rename()]
    private Material mActivationdMaterial;

    // Basic material for button
    [SerializeField]
    [Rename()]
    private Material mBasicMaterial;

    //Trail renderer
    private TrailRenderer mTrailRenderer;

    // Minimal distance needed between starting position and cursor position to consider it activated
    private float mThreshold = 5e-4f;

    // Start position of the mFillingCursor
    private Vector3 mStartPosition = new Vector3(0, .5f, -0.5f);

    // timer
    private float mTimer = 0;

    // is coroutine instance running
    private bool mIsCoroutineRunning = false;

    // get activated when the user look away
    private bool mIsDelayed = false;

    // get activated when the user look at the button
    private bool mIsLookedAt;

    public bool IsLookedAt
    {
        get => mIsLookedAt;
        set => mIsLookedAt = value;
    }

    private const float WAITING_CONST = 0.35f;


    private void Start()
    {
        mFillingCursor.localPosition = mStartPosition;
        mTrailRenderer = mFillingCursor.GetComponent<TrailRenderer>();
    }

    void Update()
    {
        // User looks at the button
        if (IsLookedAt && !mIsDelayed)
        {
            // set trail renderer parameters
            if (!mTrailRenderer.enabled)
            {
                mTrailRenderer.time = mTiming;
                mTrailRenderer.enabled = true;
            }

            // Spin the object around the center of the button.
            mFillingCursor.RotateAround(transform.localPosition, -Vector3.forward, 360 * Time.deltaTime / mTiming);

            // increase timer value
            mTimer += Time.deltaTime;

            // when cursor fill up 1 tour
            if (mTiming - mTimer < mThreshold)
            {
                // reset cursor pos
                ResetCursor();

                // invoke button events
                StartCoroutine(ChangeButtonBackground());
                mInteractable.OnClick.Invoke();
            }
        }
    }

    private void ChangeCursorColor(Material material, MeshRenderer meshRenderer)
    {
        meshRenderer.material = material;
    }

    public void ResetCursor()
    {
        // set is delayed
        mIsDelayed = true;

        // reset timer
        mTimer = 0;

        // reset position of cursor
        mFillingCursor.localPosition = mStartPosition;

        // reset trail renderer parameters
        mTrailRenderer.time = 0;
        mTrailRenderer.enabled = false;

        if (!mIsCoroutineRunning)
        {
            // start coroutine
            StartCoroutine(IsDelayCoroutine());
        }
    }


    private IEnumerator IsDelayCoroutine()
    {
        mIsCoroutineRunning = true;

        // change material color
        ChangeCursorColor(mDisabledMaterial, mFillingCursor.GetComponent<MeshRenderer>());

        // wait a given time
        yield return new WaitForSeconds(2 * mTiming);

        // reset material color
        ChangeCursorColor(mEnabledMaterial, mFillingCursor.GetComponent<MeshRenderer>());

        mIsDelayed = false;
        mIsCoroutineRunning = false;
    }

    private IEnumerator ChangeButtonBackground()
    {
        for (int i = 0; i < 2; i++)
        {
            // change quad color
            ChangeCursorColor(mActivationdMaterial, mFillingCursor.parent.GetComponent<MeshRenderer>());

            // wait a given time
            yield return new WaitForSeconds(mTiming / 8);

            // change quad color
            ChangeCursorColor(mBasicMaterial, mFillingCursor.parent.GetComponent<MeshRenderer>());

            // wait a given time
            yield return new WaitForSeconds(mTiming / 8);
        }
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    private void OnDestroy()
    {
        StopAllCoroutines();
    }
}
