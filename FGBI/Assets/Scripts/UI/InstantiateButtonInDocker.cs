﻿using Microsoft.MixedReality.Toolkit.UI;
using Microsoft.MixedReality.Toolkit.Utilities;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(GridObjectCollection))]
public class InstantiateButtonInDocker : MonoBehaviour
{
    [SerializeField]
    [Rename]
    private GameObject mButtonGameObject;

    [SerializeField]
    [Rename]
    private Transform mDockerBackground;

    private int mChildNumber;
    private GridObjectCollection mGridObjectCollection;
    private List<GameObject> mButtonGameObjectList = new List<GameObject>();
    private float mAnimationSpeed = 5;
    private CREATE_OR_DELETE mEnum;

    private enum CREATE_OR_DELETE
    {
        create,
        delete
    }
    // Start is called before the first frame update
    void Start()
    {
        mGridObjectCollection = GetComponent<GridObjectCollection>();

        StartCoroutine(UpdateViewCoroutine());
    }


    public void DeleteButton()
    {

        GameObject gameObject = mButtonGameObjectList[mButtonGameObjectList.Count - 1];
        mButtonGameObjectList.Remove(gameObject);
        GameObject.Destroy(gameObject);

        UpdateView(CREATE_OR_DELETE.delete);
    }

    public void CreateButton()
    {
        GameObject gameObject = Instantiate(mButtonGameObject, transform);
        gameObject.SetActive(true);
        mButtonGameObjectList.Add(gameObject);

        UpdateView(CREATE_OR_DELETE.create);
    }

    public void CreateButton(UnityEngine.Events.UnityAction call)
    {
        CreateButton();
        GameObject gameObject = mButtonGameObjectList[mButtonGameObjectList.Count - 1];
        gameObject.GetComponent<ButtonConfigHelper>().OnClick.AddListener(call);
    }


    private void UpdateView(CREATE_OR_DELETE status)
    {
        if (status == CREATE_OR_DELETE.create)
            mChildNumber++;
        else
            mChildNumber--;

        StartCoroutine(UpdateViewCoroutine());
    }

    private IEnumerator UpdateViewCoroutine()
    {
        yield return new WaitForUpdate();
        StopAllCoroutines();
        if (mChildNumber == 0)
            StartCoroutine(ScaleCoroutine(GetScaleY(), 0.5f));
        else
            StartCoroutine(ScaleCoroutine(GetScaleY(), mChildNumber));

        mGridObjectCollection.UpdateCollection();
    }

    private void UpdateScaleY(float y)
    {
        mDockerBackground.localScale = new Vector3(1, y, 1);
    }

    private float GetScaleY()
    {
        return mDockerBackground.localScale.y;
    }

    private IEnumerator ScaleCoroutine(float previousScale, float newScale)
    {
        float scale = previousScale;
        if (previousScale < newScale)
        {
            while (scale < newScale)
            {
                scale += Time.deltaTime * mAnimationSpeed;
                UpdateScaleY(scale);
                yield return null;
            }

        }
        else
        {
            while (scale > newScale)
            {
                scale -= Time.deltaTime * mAnimationSpeed;
                UpdateScaleY(scale);
                yield return null;
            }
        }
    }
}
