﻿using UnityEngine;

/// <summary>
/// This class allows to set a attribute [Rename()] or [Rename(string)] in order to rename a field (mostly used for private serialized fields) in the Inspector.
/// 
/// Note: This does not work to display objects like events in the Inspector.
/// </summary>
public class RenameAttribute : PropertyAttribute
{
    public string newName
    {
        get; private set;
    }
    public RenameAttribute()
    {
    }

    public RenameAttribute(string name)
    {
        newName = name;
    }
}