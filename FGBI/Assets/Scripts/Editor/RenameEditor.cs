﻿using UnityEditor;
using UnityEngine;

/// <summary>
/// It uses the RenameAttribute class to create a GUIContent with the new provided name.
/// </summary>
[CustomPropertyDrawer(typeof(RenameAttribute))]
public class RenameEditor : PropertyDrawer
{
    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        string newName = (attribute as RenameAttribute).newName;
        if (newName == null)
        {
            newName = property.displayName.Substring(2);
        }

        EditorGUI.PropertyField(position, property, new GUIContent(newName));
    }
}