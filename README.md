# Purpose
The purpose of this repo is to have a blank legacy MRTK application, using Unity v2019.4.28f1 and MRTK Foundation v2.7.0, which demonstrably built onto Hololens 2, in order to be able to begin projects more quickly.

# Getting started
1. Clone the repo
2. Using either cmd or powershell, in the root directory, execute
```
node Scripts\init.js
```